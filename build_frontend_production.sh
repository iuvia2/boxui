#!/usr/bin/env bash

echo "Need root permissions to fix permissions after building"

sudo echo 
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
docker-compose run --rm -v "$DIR"/boxui/static:/usr/local/app/dist vue npm run build -- --no-clean
sudo chown -R $USER "$DIR"/boxui/static

