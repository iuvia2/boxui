from drf_yasg.utils import swagger_auto_schema


def swagger(**kwargs):
    """Uses swagger_auto_schema but adds the correct serializer class to
    the request_body when it is overriden via @action and similar
    other decorators. This means that @swagger must be above all other
    decorators of the function in order to work properly.

    """
    def decorator(view_method):
        if hasattr(view_method, 'kwargs') and 'serializer_class' in view_method.kwargs:
            kwargs['request_body'] = view_method.kwargs['serializer_class']
        return swagger_auto_schema(**kwargs)(view_method)
    return decorator
