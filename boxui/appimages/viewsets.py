from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet

from config.swagger import swagger
from .filters import LauncherUseTypeFilter, LauncherListPagination
from .models import AppImage, AppLauncher, IuviaPlatformApp, LauncherUse
from .renderers import ImageRenderer
from .serializers import (
    AppImageSerializer,
    AppLauncherSerializer,
    IuviaPlatformAppSerializer,
    LauncherUseSerializer
)
from . import tasks


class IuviaPlatformAppViewSet(ReadOnlyModelViewSet):
    """
    List all available platform applications
    """

    serializer_class = IuviaPlatformAppSerializer
    queryset = IuviaPlatformApp.objects.all()

    def create(self, request):
        appimages = tasks.refresh_installed_appimages()
        serializer = IuviaPlatformAppSerializer(appimages, many=True, context={'request': request})
        return Response(serializer.data)


class AppImageViewSet(ReadOnlyModelViewSet):
    """
    List all available appimages, read or delete from database
    """

    serializer_class = AppImageSerializer
    queryset = AppImage.objects.all()

    def get_object(self):
        filter_kwargs = {
            'codename': self.kwargs['codename'],
            'version': self.kwargs['version']
        }
        return get_object_or_404(self.queryset, **filter_kwargs)

    @action(detail=True)
    def install(self, request, codename=None, version=None):
        obj: AppImage = self.get_object()
        return Response(tasks.install_appimage(obj))

    @action(detail=True)
    def uninstall(self, request, codename=None, version=None):
        obj: AppImage = self.get_object()
        return Response(tasks.uninstall_appimage(obj))


class AppLauncherViewSet(ListModelMixin, GenericViewSet):
    """
    List all available launcher from installed AppImages
    filters:
        - ?search=<str:search_string>
        - ?use_type=<str:use_type>
    """

    serializer_class = AppLauncherSerializer
    filter_backends = [SearchFilter, LauncherUseTypeFilter, OrderingFilter]
    search_fields = ['l10n__name', 'l10n__description']
    ordering_fields = ['users_use__timestamp']
    pagination_class = LauncherListPagination

    def get_queryset(self):
        return AppLauncher.objects.filter(appimage__install_state=AppImage.STATE_INSTALLED)

    @swagger(
        request_body=LauncherUseSerializer,
        responses={
            200: "AppLauncher use added successfully",
            400: "Wrong format of request data",
            404: "AppLauncher not found"
        }
    )
    @action(
        detail=True,
        methods=('POST',)
    )
    def use(self, request, pk=None):
        serializer = LauncherUseSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        launcher = self.get_object()
        if not launcher:
            return Response({'error': 'AppLauncher not found'}, status=status.HTTP_404_NOT_FOUND)

        obj = None
        if serializer.validated_data['use_type'] == LauncherUse.USE_ACCESS:
            # Add new user-launcher access entry
            obj = LauncherUse.objects.create(
                user=request.user,
                launcher=launcher,
                use_type=serializer.validated_data['use_type'],
            )

        else:
            # Add or remove user-launcher <type> entry depending on value
            if serializer.validated_data['value']:
                obj, created = LauncherUse.objects.update_or_create(
                    user=request.user,
                    launcher=launcher,
                    use_type=serializer.validated_data['use_type'],
                )
            else:
                LauncherUse.objects.filter(
                    user=request.user,
                    launcher=launcher,
                    use_type=serializer.validated_data['use_type'],
                ).delete()

        if obj:
            serializer = LauncherUseSerializer(obj)
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response({}, status=status.HTTP_200_OK)


class IconView(APIView):
    renderer_classes = (ImageRenderer,)

    def get(self, request, codename, version, *args, **kwargs):
        size = kwargs.get("size", None)
        try:
            app = AppImage.objects.get(codename=codename, version=version)
        except AppImage.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(app.icon(size), content_type="image/png")


class LauncherIconView(APIView):
    renderer_classes = (ImageRenderer,)

    def get(self, request, *args, **kwargs):
        launcherpk = kwargs.get("launcherpk", None)
        size = kwargs.get("size", None)
        try:
            launcher = AppLauncher.objects.get(pk=launcherpk)
        except AppLauncher.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        return Response(launcher.icon(size), content_type="image/png")
