from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AppImagesConfig(AppConfig):
    name = "boxui.appimages"
    verbose_name = _("AppImages")
