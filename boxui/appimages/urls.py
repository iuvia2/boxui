from django.urls import path
from rest_framework.routers import DefaultRouter

from boxui.appimages import viewsets

app_name = "appimages"

appimages_read = viewsets.AppImageViewSet.as_view({'get': 'retrieve'})
appimages_install = viewsets.AppImageViewSet.as_view({'get': 'install'})
appimages_uninstall = viewsets.AppImageViewSet.as_view({'get': 'uninstall'})

urlpatterns = [
    path('<str:codename>-<str:version>/', appimages_read, name='appimages_read'),
    path('<str:codename>-<str:version>/install/', appimages_install, name='appimages_install'),
    path('<str:codename>-<str:version>/uninstall/', appimages_uninstall, name='appimages_uninstall'),
    path('<str:codename>-<str:version>/icon/<int:size>/', viewsets.IconView.as_view(), name='appimage_icon'),
    path('_/launchers/<str:launcherpk>/icon/<int:size>/', viewsets.LauncherIconView.as_view(), name='launcher_icon'),
]

router = DefaultRouter()
router.register(r'_/launchers', viewsets.AppLauncherViewSet, basename='launchers')
router.register(r'_/platformapps', viewsets.IuviaPlatformAppViewSet, basename='platformapps')
urlpatterns += router.urls
