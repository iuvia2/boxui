import json
import os
import uuid

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField, JSONField
from django.core.exceptions import ObjectDoesNotExist
from django.db import models, transaction
from django.urls import reverse
from django.utils.translation import get_language

from iuvia.appimages.lifecycle import LifecycleManager
from iuvia.appimages import AppImageOps

from .mixins import ProtoBufMixin
from .validators import validate_action_format


class AppDeveloper(ProtoBufMixin):
    """ An authorized developer of AppImages
    """

    public_key = models.BinaryField(null=True)
    name = models.CharField(max_length=255, null=True)
    primary_email = models.CharField(max_length=255, null=True)
    trusted_until = models.DateTimeField(null=True)


class IuviaPlatformApp(ProtoBufMixin):
    """ An compatible app available for it's installation in the IUVIA
    platform. Uniquely identified by a codename, might have multiple
    AppImages with different versions of the same app available.
    """

    pb_track_fields = ['codename']

    codename = models.CharField(max_length=255, primary_key=True)
    auto_update = models.BooleanField(default=True)
    developer = models.ForeignKey(
        AppDeveloper,
        on_delete=models.SET_NULL,
        related_name='app_set',
        null=True,
    )
    latest = models.OneToOneField(
        'AppImage',
        on_delete=models.SET_NULL,
        related_name='latest_of_app',
        null=True,
    )
    installed = models.OneToOneField(
        'AppImage',
        on_delete=models.SET_NULL,
        related_name='installed_of_app',
        null=True,
    )

    def __str__(self):
        return "IuviaPlatformApp {codename}".format(codename=self.codename)

    @classmethod
    def get_or_create_from_ops(cls, ops, save=True):
        """
        Returns the models.IuviaPlatformApp that reflects the metadata from this AppImageOps.

        When we read an AppImageOpts from protobuf, what we should do is:
        1. Check if there is a IuviaPlatformApp identified by codename
            1.a. IF THERE IS:
                1.a.1. Check if AppDeveloper is the same public key as message.developer
                    1.a.1.a. IF NOT, exit with error
                1.a.2. Check if there is AppImage identified by version
                    1.a.2.a. IF THERE IS: return IPA
                    1.a.2.b. ELSE: go to [2]
            1.b. ELSE:
                1.b.1. Get or create developer
                1.b.2. Create new IuviaPlatformApp: from_pb
        2. Create new AppImage: from_pb
            - AppImage.from_pb should take care of creating all the
              child objects (launchers, resources, requirements, translations, etc.)
        3. Fill up aditional attributes: path, signature, auto_update, etc.
        4. Add AppImage to IuviaPlatformApp
        5. Update IuviaPlatformApp.latest with latest version of AppImage

        :param save: If true, the model is saved on the db
            before returning if it did not exist (along with other sub-objects)
        :returns: a models.AppImage with the metadata filled
        """

        metadata = ops.get_metadata_pb()

        # 1. Check if there is a platform app identified by this codename
        try:
            obj = cls.objects.get(codename=metadata.codename)
            if bytes(obj.developer.public_key) != metadata.developer:
                raise DeveloperNotMatching

        except cls.DoesNotExist:
            obj = cls()
            obj.from_pb(metadata)
            obj.developer, c = AppDeveloper.objects.get_or_create(public_key=metadata.developer)

        # Now we have the IPA with its developer
        try:
            # Check if the AppImage in this version is already saved
            obj.appimages.get(version=metadata.version)
            return obj

        except AppImage.DoesNotExist:
            app = AppImage()

            # Fill up the aditional fields (for AppImage)
            app.signature = ops.get_signature()
            app.path = ops.path

            # Add AppImage to IuviaPlatformApp
            app.codename = obj

            saving_list = app.from_pb(metadata)

            with transaction.atomic():
                # bring IuviaPlatformApp initial saving inside the transaction
                # so that we don't create an empty IPA object when the AppImage
                # metadata fails with a ValidationError
                obj.save()
                for msg in saving_list:
                    msg.full_clean()
                    msg.save()

            # Update latest IuviaPlatformApp version available
            obj.latest = obj.appimages.order_by("-version").first()
            obj.save()

            return obj


class AppImage(ProtoBufMixin):
    """ An installed AppImage
    """

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["codename", "version"], name="unique_codename_version"
            ),
        ]

    pb_track_fields = [
        'metadata_version',
        'version',
        'certificate',
        'requires',
        'expected_resources',
        'max_resources',
        'launchers',
        'l10n',
        'l10n_default',
        'license_code',
        'license_text',
        'icon_sizes',
        'icon_locations',
    ]

    STATE_UNINSTALLED = 0x00
    STATE_INSTALLED = 0x01
    STATE_INSTALL_PENDING = 0x02
    STATE_INSTALL_FAILED = 0x04
    STATE_INSTALLING = 0x07
    STATE_UNINSTALLING = 0x20
    STATE_UNINSTALL_FAILED = 0x40
    STATE_UPDATING = 0x17

    INSTALL_STATES = (
        (STATE_UNINSTALLED, 'UNINSTALLED'),
        (STATE_INSTALLED, 'INSTALLED'),
        (STATE_INSTALL_PENDING, 'INSTALL PENDING'),
        (STATE_INSTALL_FAILED, 'INSTALL FAILED'),
        (STATE_INSTALLING, 'INSTALLING'),
        (STATE_UNINSTALLING, 'UNINSTALLING'),
        (STATE_UNINSTALL_FAILED, 'UNINSTALL FAILED'),
        (STATE_UPDATING, 'UPDATING'),
    )

    uid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    codename = models.ForeignKey(
        IuviaPlatformApp,
        on_delete=models.CASCADE,
        related_name='appimages',
    )
    metadata_version = models.PositiveIntegerField(null=True)
    version = models.CharField(max_length=32, null=True)
    certificate = models.BinaryField(null=True)
    icon_sizes = ArrayField(models.IntegerField(default=0), default=list, null=True, blank=True)
    icon_locations = JSONField(null=True, blank=True, default=list)
    l10n_default = models.CharField(max_length=32, null=True)
    license_code = models.CharField(max_length=32, null=True)
    license_text = models.TextField(null=True)

    # Aditional attributes
    install_state = models.IntegerField(default=0, choices=(INSTALL_STATES))
    path = models.CharField(max_length=255, null=True, unique=True)
    signature = models.BinaryField(null=True)

    def __str__(self):
        return "AppImage {codename} (v{version})".format(
            codename=self.codename.codename, version=self.version,
        )

    @property
    def id(self):
        return "{codename}-{version}".format(
            codename=self.codename.codename,
            version=self.version
        )

    @property
    def is_installed(self):
        return self.install_state == self.STATE_INSTALLED

    @property
    def ops(self):
        return AppImageOps(self.path)

    @property
    def subdomain(self):
        last_dot = self.codename.codename.rfind(".")
        return self.codename.codename[last_dot+1:].lower()

    @property
    def lang(self):
        # Order of preference for translated fields:
        # 1. lang = get_language()
        # 2. lang = self.l10n_default
        # 3. lang = whatever is available
        available_languages = [trans.lang for trans in self.l10n.all()]
        if get_language() in available_languages:
            return get_language()
        elif self.l10n_default in available_languages:
            return self.l10n_default
        else:
            return available_languages[0] if len(available_languages) > 0 else None

    @property
    def literals_l10n(self):
        if self.lang:
            try:
                return self.l10n.get(lang=self.lang)
            except ObjectDoesNotExist:
                pass
        return None

    @property
    def name_l10n(self):
        if self.literals_l10n:
            return self.literals_l10n.name
        return None

    @property
    def short_description_l10n(self):
        if self.literals_l10n:
            return self.literals_l10n.short_description
        return None

    @property
    def long_description_l10n(self):
        if self.literals_l10n:
            return self.literals_l10n.long_description
        return None

    def _get_icon_url(self, size):
        return reverse('appimages:appimage_icon', kwargs={
            'codename': self.codename.codename,
            'version': self.version,
            'size': size
        })

    @property
    def icon_urls(self):
        icon_dict = dict(json.loads(self.icon_locations))
        icon_dict = {key: self._get_icon_url(key) for (key, value) in icon_dict.items()}
        icon_dict['default'] = self._get_icon_url(0)
        return icon_dict

    def icon(self, size):
        if len(self.icon_sizes) == 0:
            f = open(os.path.join(settings.STATIC_PATH, "images", "app.png"), "rb")
            icon = f.read()
            f.close()
            return icon

        if size not in self.icon_sizes:
            size = sorted(self.icon_sizes, reverse=True)[0]
        key = str(size)
        icon_path = dict(json.loads(self.icon_locations))[key]
        icon = self.ops.get_icon(icon_path)
        return icon

    def install(self):
        self.install_state = 0x07
        self.save()
        try:
            self.lifecycle_manager.install(self.subdomain)
            self.install_state = 0x01
            self.save()
        except RuntimeError:
            self.install_state = 0x04
            self.save()

    def uninstall(self):
        self.install_state = 0x20
        self.save()
        try:
            self.lifecycle_manager.uninstall(self.subdomain)
            self.install_state = 0x00
            self.save()
        except RuntimeError:
            self.install_state = 0x40
            self.save()

    @property
    def lifecycle_manager(self):
        return LifecycleManager.create(self.ops)

    @property
    def canonical_app_base_url(self):
        return '{protocol}://{subdomain}.{domain}:{port}'.format(
            protocol=settings.BOXUI_FRONTEND_PROTOCOL,
            subdomain=self.subdomain,
            domain=settings.DEFAULT_APP_DOMAIN,
            port=settings.DEFAULT_APP_PORT
        )


class LocalizedAppStrings(ProtoBufMixin, models.Model):
    """ i18n For AppImage objects
    Attributes:
        lang
        name
        short_description
        long_description
    """

    appimage = models.ForeignKey(
        AppImage,
        on_delete=models.CASCADE,
        related_name='l10n',
    )
    lang = models.CharField(max_length=32, null=True, db_index=True)
    name = models.CharField(max_length=255, null=True)
    short_description = models.TextField(null=True)
    long_description = models.TextField(null=True)


class Requirement(ProtoBufMixin, models.Model):
    """ Requirements for the AppImage
    Attributes:
        codename
        min_version
        max_version
    """

    appimage = models.ForeignKey(
        AppImage,
        on_delete=models.CASCADE,
        related_name='requires',
    )
    codename = models.CharField(max_length=255, null=True)
    min_version = models.CharField(max_length=32, null=True)
    max_version = models.CharField(max_length=32, null=True)


class PlatformResources(ProtoBufMixin, models.Model):
    """ Expected CPU and RAM resources for an AppImage to work
    Attributes:
        cpu
        ram
    """

    class Meta:
        abstract = True

    cpu = models.CharField(max_length=32, null=True)
    ram = models.CharField(max_length=32, null=True)


class MaxPlatformResources(PlatformResources):
    appimage = models.OneToOneField(
        AppImage,
        on_delete=models.CASCADE,
        related_name='max_resources',
        primary_key=True,
    )


class ExpectedPlatformResources(PlatformResources):
    appimage = models.OneToOneField(
        AppImage,
        on_delete=models.CASCADE,
        related_name='expected_resources',
        primary_key=True,
    )


class AppLauncher(ProtoBufMixin, models.Model):
    """ A Launcher associated with an installed AppImage
    Attributes:
        platform
        on_activate
        conditions
        l10n
    """
    uid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    appimage = models.ForeignKey(
        AppImage,
        on_delete=models.CASCADE,
        related_name='launchers',
    )
    platform = models.CharField(max_length=32, null=True)
    on_activate = models.CharField(max_length=32, null=True, db_index=True, validators=[validate_action_format])
    icon_sizes = ArrayField(models.IntegerField(default=0), default=list, null=True, blank=True)
    icon_locations = JSONField(null=True, blank=True, default=list)

    @property
    def lang(self):
        # Order of preference for translated fields:
        # 1. lang = get_language()
        # 2. lang = self.appimage.l10n_default
        # 3. lang = whatever is available
        available_languages = [trans.lang for trans in self.l10n.all()]
        if get_language() in available_languages:
            return get_language()
        elif self.appimage.l10n_default in available_languages:
            return self.appimage.l10n_default
        else:
            return available_languages[0] if len(available_languages) > 0 else None

    @property
    def literals_l10n(self):
        if self.lang:
            try:
                return self.l10n.get(lang=self.lang)
            except ObjectDoesNotExist:
                pass
        return None

    @property
    def name_l10n(self):
        if self.literals_l10n:
            return self.literals_l10n.name
        return None

    @property
    def description_l10n(self):
        if self.literals_l10n:
            return self.literals_l10n.description
        return None

    @property
    def action_variables(self):
        return {
            'CANONICAL_APP_BASE_URL': self.appimage.canonical_app_base_url
        }

    @property
    def action_type(self):
        try:
            action_type, action_params = self.on_activate.split(":", maxsplit=1)
            return action_type
        except ValueError:
            return ''

    @property
    def action_params(self):
        try:
            action_type, action_params = self.on_activate.split(":", maxsplit=1)
            return action_params.format(**self.action_variables)
        except ValueError:
            return ''

    def _get_icon_url(self, size):
        return reverse('appimages:launcher_icon', kwargs={
            'launcherpk': self.pk,
            'size': size
        })

    @property
    def icon_urls(self):
        icon_dict = dict(json.loads(self.icon_locations))
        icon_dict = {key: self._get_icon_url(key) for (key, value) in icon_dict.items()}
        icon_dict['default'] = self._get_icon_url(0)
        return icon_dict

    def icon(self, size):
        if len(self.icon_sizes) == 0:
            f = open(os.path.join(settings.STATIC_PATH, "images", "app.png"), "rb")
            icon = f.read()
            f.close()
            return icon

        key = str(size) if size in self.icon_sizes else str(self.icon_sizes[0])
        icon_path = dict(json.loads(self.icon_locations))[key]
        icon = self.appimage.icon(icon_path)
        return icon


class LauncherCondition(ProtoBufMixin, models.Model):
    """ Conditions for launcher to be displayed
    Attributes:
        operator
    """

    launcher = models.ForeignKey(
        AppLauncher,
        on_delete=models.CASCADE,
        related_name='conditions',
    )
    operator = models.CharField(max_length=255, null=True)


class LocalizedLauncherStrings(ProtoBufMixin, models.Model):
    """ i18n for AppLauncher objects
    Attributes:
        lang
        name
        description
    """

    launcher = models.ForeignKey(
        AppLauncher,
        on_delete=models.CASCADE,
        related_name='l10n',
    )
    lang = models.CharField(max_length=32, null=True, db_index=True)
    name = models.CharField(max_length=255, null=True)
    description = models.TextField(null=True)


class LauncherUse(models.Model):
    """
    Saves users' use of launchers:
    favorites, hidden and last accessed
    """

    USE_ACCESS = 0x00
    USE_FAVORITE = 0x01
    USE_HIDDEN = 0x02
    USE_TYPES = (
        (USE_ACCESS, 'access'),
        (USE_FAVORITE, 'favorite'),
        (USE_HIDDEN, 'hidden'),
    )

    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        related_name='launchers_use'
    )
    launcher = models.ForeignKey(
        AppLauncher,
        on_delete=models.CASCADE,
        related_name='users_use'
    )
    use_type = models.IntegerField(default=USE_ACCESS, choices=(USE_TYPES))
    timestamp = models.DateTimeField(auto_now=True)


class DeveloperNotMatching(Exception):
    pass
