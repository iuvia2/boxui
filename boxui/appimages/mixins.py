from django.core.exceptions import FieldDoesNotExist
from django.contrib.postgres.fields import ArrayField, JSONField
from django.db import models

from . import fields


class ProtoBufMixin(models.Model):

    pb_track_fields = None
    pb_to_dj_serializers = {
        ArrayField: fields._pb_arrayField_to_dj,
        JSONField: fields._pb_jsonField_to_dj,
    }

    class Meta:
        abstract = True

    def from_pb(self, pb):
        """
        Transform a parsed protobuf message to a django model
        """
        saving_list = [self]

        # We go through all the list of parameters that the protobuf message has
        for f, v in pb.ListFields():
            # We check that the field exists in the self.pb_track_fields
            # or self.pb_track_fields is undefined (which means that all PB
            # fields are to be saved into the django model)
            if self.pb_track_fields is None or f.name in self.pb_track_fields:
                # We check that the field exists in the django model
                try:
                    dj_field = self._meta.get_field(f.name)
                except FieldDoesNotExist:
                    dj_field = None

                # We check whether it's a value or a relation:
                #   Value or Array: f.message_type == None
                #   JSON or Map: f.message_type != None & dj_field.is_relation == False
                #   Relation: f.message_type != None & dj_field.is_reation == True
                if dj_field is not None:
                    if f.message_type is not None and dj_field.is_relation:
                        # Saves one-to-one or one-to-many relations
                        saving_list = saving_list + self._save_pb_relation(v, f.name, dj_field)
                    else:
                        # Saves values, JSON and Array fields:
                        self._save_pb_value(v, f, type(dj_field))

        return saving_list

    def _get_pb_to_dj_function(self, dj_field_type):
        """
        Given a field, return the functions that serialize the protobuf
        value if necessary and store it in the django attribute
        """
        return self.pb_to_dj_serializers.get(dj_field_type, fields._pb_value_to_dj)

    def _save_pb_value(self, pb_value, dj_field, dj_field_type):
        """
        Given a protobuf field and value, save this to the given
        field of the instance. The functions returned must be called with
        the following attributes:
            - dj_obj: the django model instance
            - dj_field: the field object where the attribute must be saved
            - pb_value: the value from the protobuf message
        """
        pb_to_dj_function = self._get_pb_to_dj_function(dj_field_type)
        pb_to_dj_function(self, dj_field, pb_value)

    def _save_pb_relation(self, pb_value, dj_field_name, dj_field):
        """
        Given a protobuf field and MessageDescriptor or map value, create
        the child objects and save this into the given field of the instance
        """
        saving_list = []
        if dj_field.one_to_many:
            for item in pb_value:
                related_obj = dj_field.related_model()
                saving_list = saving_list + related_obj.from_pb(item)
                setattr(related_obj, dj_field.remote_field.name, self)

        if dj_field.one_to_one:
            related_obj = dj_field.related_model()
            saving_list = saving_list + related_obj.from_pb(pb_value)
            setattr(self, dj_field_name, related_obj)

        return saving_list
