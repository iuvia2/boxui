import json


def _pb_value_to_dj(dj_obj, dj_field, pb_value):
    setattr(dj_obj, dj_field.name, pb_value)


def _pb_arrayField_to_dj(dj_obj, dj_field, pb_value):
    setattr(dj_obj, dj_field.name, list(pb_value))


def _pb_jsonField_to_dj(dj_obj, dj_field, pb_value):
    setattr(dj_obj, dj_field.name, json.dumps(dict(pb_value)))
