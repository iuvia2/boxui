import re

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_action_format(value):
    """
    Validator function for the field AppLauncher.on_activate
    Checks that the format of the on_activate string follows the
    pattern ACTION_TYPE:ACTION_PARAMETERS
    """

    ACTION_REGEX = r"(?P<action>^[\w]*)\:(?P<params>.*)"

    if not re.match(ACTION_REGEX, value):
        raise ValidationError(
            _('%(value)s is not a valid action format. Please follow the structure ACTION_TYPE:ACTION_PARAMETERS'),
            params={'value': value},
        )
