import os

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

import ldap
import ldap.modlist

from user_agents import parse as ua_parse


LDAP_OBJECTCLASSES = ('account', 'extensibleObject')


class KnoxSessionOrigin(models.Model):
    token = models.OneToOneField(
        'knox.AuthToken',
        on_delete=models.CASCADE,
        primary_key=True,
        related_name='session',
    )
    user_agent = models.CharField(max_length=255)

    # Future: Add session name for sessions that can be named (allow
    # renames in the future?)
    name = models.CharField(max_length=120, blank=True)

    # Future: add future support for storing location where the
    # session was established via GeoIP
    location = models.CharField(max_length=255, blank=True)

    @property
    def device_type(self):
        ua = ua_parse(self.user_agent)
        if ua.is_mobile:
            return 'mobile'
        elif ua.is_tablet:
            return 'tablet'
        elif ua.is_pc:
            return 'pc'
        else:
            return 'unknown'


class User(AbstractUser):
    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = models.CharField(_("Name of User"), blank=True, max_length=255)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})


class LDAPUserManager(object):
    def __init__(self):
        self._ldap = None

    @property
    def ldap(self):
        if self._ldap is None:
            self._ldap = ldap.initialize(settings.AUTH_LDAP_SERVER_URI)
        return self._ldap

    def bind(self):
        who = settings.AUTH_LDAP_BIND_DN
        cred = settings.AUTH_LDAP_BIND_PASSWORD
        self.ldap.simple_bind_s(who, cred)

    def _get_groups(self, dn):
        return self.ldap.search_s(
            settings.AUTH_LDAP_ROOT_DN,
            ldap.SCOPE_SUBTREE,
            filterstr=Filter.And(objectClass='groupOfNames', member=dn)
        )

    def all(self):
        self.bind()
        results = self.ldap.search_st(
            settings.AUTH_LDAP_ROOT_DN,
            ldap.SCOPE_SUBTREE,
            filterstr=Filter.And(objectClass=LDAP_OBJECTCLASSES[0]),
            timeout=30
        )
        return [LDAPUser(dn=r[0], **r[1], groups=self._get_groups(r[0])) for r in results]

    def find(self, dn=None, pk=None):
        """Find a user by DN (fully qualified, e.g.,
        "cn=admin,ou=users,dc=base") or by PK (RDN value; e.g.,
        "admin")

        """
        self.bind()

        if pk:
            results = self.ldap.search_st(
                settings.AUTH_LDAP_ROOT_DN,
                ldap.SCOPE_SUBTREE,
                filterstr=Filter.And(objectClass=LDAP_OBJECTCLASSES[0], uid=pk),
                timeout=30
            )
        elif dn:
            results = self.ldap.search_st(
                dn,
                ldap.SCOPE_BASE,
                timeout=30
            )

        results = [LDAPUser(dn=r[0], **r[1], groups=self._get_groups(r[0])) for r in results]
        if len(results) > 0:
            return results[0]

    def delete(self, dn=None, pk=None):
        self.bind()

        # Remove user
        if pk:
            dn = '{rdn}={username},{trail}'.format(
                rdn='uid',
                username=pk,
                trail=settings.AUTH_LDAP_CREATEUSER_SUBTREE,
            )
        self.ldap.delete_s(dn)

        # Remove user from groups
        groups = self._get_groups(dn)
        for group in groups:
            self.ldap.modify_s(group[0], [(ldap.MOD_DELETE, 'member', [dn.encode('utf-8')])])

        return True

    def create(self, dn, **kwargs):
        self.bind()
        # Check whether user exists in LDAP
        # FIXME Should we search by dn or by username
        #       as it was previously done in auth.py?
        try:
            self.ldap.search_st(
                dn,
                ldap.SCOPE_BASE,
                timeout=30
            )
            raise LDAPUserExists

        except ldap.LDAPError:
            kwargs = {k: v for k, v in kwargs.items() if v}
            modlist = self.dict2addModlist(kwargs)
            self.ldap.add_s(dn, modlist)
            return self.find(dn)

    def deactivate(self, dn):
        group = "cn=active,ou={},ou=groups,{}".format(
            settings.AUTH_LDAP_APP_NAME,
            settings.AUTH_LDAP_ROOT_DN
        )
        return self.ldap.modify_s(
            group,
            [
                (ldap.MOD_DELETE, 'member', [dn.encode('utf-8')])
            ]
        )

    def activate(self, dn):
        group = "cn=active,ou={},ou=groups,{}".format(
            settings.AUTH_LDAP_APP_NAME,
            settings.AUTH_LDAP_ROOT_DN
        )
        return self.ldap.modify_s(
            group,
            [
                (ldap.MOD_ADD, 'member', [dn.encode('utf-8')])
            ]
        )

    def update(self, instance, **kwargs):
        kwargs = {k: v for k, v in kwargs.items() if v}
        modlist = self.dict2modifyModlist(instance, kwargs)
        return self.ldap.modify_s(instance.dn, modlist)

    def change_password(self, instance, new_password):
        return self.ldap.passwd_s(
            instance.dn,
            None,
            new_password
        )

    def dict2addModlist(self, dict):
        d = {}
        for k, v in dict.items():
            if type(v) is not list:
                d[k] = [v.encode('utf-8')]
            else:
                d[k] = v
        return ldap.modlist.addModlist(d)

    def dict2modifyModlist(self, instance, dict):
        new_entry = {}
        old_entry = {}
        for k, v in dict.items():
            if type(v) is not list:
                if k in instance.attributes:
                    old_entry[k] = [instance.__getattr__(k).encode('utf-8')]
                new_entry[k] = [v.encode('utf-8')]
            else:
                if k in instance.attributes:
                    old_entry[k] = instance.__getattr__(k)
                new_entry[k] = v
        return ldap.modlist.modifyModlist(old_entry, new_entry)


class Filter(object):
    class NaryFilter(object):
        def __init__(self, operator):
            self.operator = operator

        def __call__(self, *args, **kwargs):
            return '({operator}({elements}))'.format(
                operator=self.operator,
                elements=")(".join(
                    ([
                        a.to_ldap() for a in args
                    ] + Filter.kwargs(**kwargs))
                )
            )

    And = NaryFilter('&')
    Or = NaryFilter('|')
    Not = NaryFilter('!')

    def kwargs(**kwargs):
        return [
            ldap.filter.filter_format('%s=%s', [key, val])
            for key, val in kwargs.items()
        ]


class LDAPUser:
    objects = LDAPUserManager()

    attr_str_unique = (
        'uid',
        'sn',
        'givenName',
        'cn',
        'preferredLanguage',
    )

    attr_str = (
        'objectClass',
        'mail',
    )

    def __init__(self, **kwargs):
        self.attributes = kwargs
        rdn = self.attributes['dn'].split('=')[0]
        if 'groups' in self.attributes:
            self.attributes['groups'] = [g[1]['cn'][0].decode('utf-8') for g in self.attributes['groups']]
        self.attributes['pk'] = self.attributes[rdn][0].decode('utf-8')

    def __dict__(self):
        return self.attributes.keys()

    def __getattr__(self, attr):
        if attr in self.attr_str_unique:
            return self.attributes[attr][0].decode('utf-8')
        elif attr in self.attr_str:
            return [v.decode('utf-8') for v in self.attributes[attr]]
        else:
            return self.attributes[attr]

    def __str__(self):
        return str(self.attributes.keys())

    @property
    def is_admin(self):
        if 'groups' in self.attributes:
            return 'superuser' in self.attributes['groups']
        return False

    @property
    def avatar_url(self):
        return reverse('users:user-users_avatar', kwargs={
            'pk': self.uid,
        })

    def avatar(self):
        f = open(os.path.join(settings.STATIC_PATH, "images", "avatar.png"), "rb")
        icon = f.read()
        f.close()
        return icon


class LDAPUserExists(Exception):
    pass
