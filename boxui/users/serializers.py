from django.conf import settings
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _, get_language
from rest_framework.serializers import (
    BooleanField,
    CharField,
    ChoiceField,
    Field as _f,
    HyperlinkedIdentityField,
    ListField,
    ModelSerializer,
    Serializer,
    SerializerMethodField,
    ValidationError,
)

from knox.models import AuthToken
from boxui.users.models import KnoxSessionOrigin, LDAPUser

__all__ = ['LDAPUserSerializer', 'LoginSerializer']
_l = ListField


class KnoxSessionOriginSerializer(ModelSerializer):
    class Meta:
        model = KnoxSessionOrigin
        fields = ['user_agent', 'name', 'location', 'device_type']


class KnoxSessionSerializer(ModelSerializer):
    session = KnoxSessionOriginSerializer()
    is_current_session = SerializerMethodField()

    def get_is_current_session(self, obj):
        token = self.context['request'].auth
        if token and token == obj:
            return True
        return False

    class Meta:
        model = AuthToken
        fields = ['token_key', 'session', 'created', 'expiry', 'is_current_session']


class LDAPUserSerializer(Serializer):
    url = HyperlinkedIdentityField(view_name='users:user-detail')
    dn = CharField(required=False, allow_null=True)
    objectClass = _l(child=CharField(required=False, allow_null=True), read_only=True, required=False, allow_empty=True)
    cn = CharField(required=False, allow_null=True)
    userPassword = _l(child=CharField(required=False), allow_empty=False, write_only=True)
    uid = CharField(required=False, allow_null=True)
    givenName = CharField(required=False, allow_null=True)
    sn = CharField(required=False, allow_null=True)
    mail = _l(child=CharField(required=False, allow_null=True), allow_empty=False)
    preferredLanguage = CharField(required=False, allow_null=True)

    is_admin = BooleanField(read_only=True)
    avatar_url = CharField(read_only=True)

    allowed_actions = SerializerMethodField(read_only=True)

    ACTIONS = {
        'delete': ['is_superuser'],
        'edit': ['is_superuser', 'is_own'],
        'activation': ['is_superuser'],
    }

    def get_allowed_actions(self, obj):
        user = self.context['request'].user
        actions = []
        if user.is_superuser:
            actions += [key for key in self.ACTIONS.keys() if 'is_superuser' in self.ACTIONS[key]]
        elif user.username == obj.uid:
            actions += [key for key in self.ACTIONS.keys() if 'is_own' in self.ACTIONS[key]]
        return set(list(actions))

    def validate_userPassword(self, data):
        if len(data) != 2:
            raise ValidationError(_('Repeat password'))
        if data[0] != data[1]:
            raise ValidationError(_('Passwords don\'t match'))
        return data[0]

    def create(self, validated_data):
        encoded_email = [s.encode('utf-8') for s in validated_data['mail']]
        user = LDAPUser.objects.create(
            '{rdn}={username},{trail}'.format(
                rdn='uid',
                username=validated_data['uid'],
                trail=settings.AUTH_LDAP_CREATEUSER_SUBTREE,
            ),
            objectClass=[b'account', b'extensibleObject'],
            userPassword=validated_data['userPassword'],
            sn=(validated_data['sn'] or " "),
            givenName=(validated_data['givenName'] or " "),
            cn="{} {}".format(validated_data['givenName'], validated_data['sn']),
            mail=encoded_email,
            preferredLanguage=get_language(),
        )
        return user

    def update(self, instance, validated_data):
        kwargs = {
            'sn': (validated_data['sn'] or " "),
            'givenName': (validated_data['givenName'] or " "),
            'cn': '{} {}'.format(validated_data['givenName'], validated_data['sn']),
            'mail': [s.encode('utf-8') for s in validated_data['mail']],
        }

        if 'userPassword' in validated_data:
            LDAPUser.objects.change_password(
                instance,
                validated_data['userPassword']
            )

        return LDAPUser.objects.update(instance, **kwargs)


class LDAPMultivaluedAttributeField(_f):
    def to_representation(self, value):
        """
        The value as externally represented and printed.
        E.g., Response(
        """
        pass

    def to_internal_value(self, data):
        """
        The value as internally represented.
        E.g., serializer.data
        """
        pass


class LoginSerializer(Serializer):
    user = CharField(required=True)
    password = CharField(required=False, style={'input_type': 'password'})
    token = CharField(required=False)
    method = ChoiceField(('password', 'token'), required=True)

    def authenticate(self, request=None):
        if not self.is_valid():
            return None

        pwfield = 'password'
        if self.data['method'] == 'token':
            pwfield = 'token'
        return authenticate(
            request=request,
            username=self.data['user'],
            password=self.data[pwfield],
        ) or authenticate(
            request=request,
            username=self.data['user'],
            token=self.data[pwfield],
        )


class UserActivationSerializer(Serializer):
    is_active = BooleanField(required=True)


class LanguageSerializer(Serializer):
    lang = CharField(required=True)
