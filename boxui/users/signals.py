from django.contrib.auth.signals import user_logged_in
from django.db.models.signals import post_delete
from django.dispatch import receiver
from knox.models import AuthToken
from .models import KnoxSessionOrigin


@receiver(post_delete, sender=KnoxSessionOrigin)
def session_delete_trigger_token_delete(sender, instance, **kwargs):
    print("Deleting instance token", instance.token)
    instance.token.delete()


@receiver(user_logged_in)
def user_logged_in_through_knox_add_sessiondata(sender, **kwargs):
    if 'request' in kwargs:
        # Test for AuthToken
        try:
            token = kwargs['token']
            request = kwargs['request']
        except KeyError:
            print("No deal")
            return
        if type(token) is AuthToken:
            print("Authenticated via token")
            print("Add session information to token")
            KnoxSessionOrigin.objects.create(
                token=token,
                user_agent=request.META['HTTP_USER_AGENT'],
                # Future: Add missing properties
            )
        else:
            print("Authenticated somehow else: ", type(token))
