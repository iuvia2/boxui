import os
import subprocess  # noqa: S404
from collections import defaultdict

import click
import iuvia.control.internal

boxui_systemd_service_template = """
[Unit]
Description=BoxUI is the Management Interface for IUVIA
Documentation=https://iuvia.io/support
Requires=network.target

[Service]
Type=notify
NotifyAccess=all
WorkingDirectory=/opt/boxui
StateDirectory=iuvia/boxui
ExecStart=gunicorn config.wsgi
EnvironmentFile=-/etc/systemd/system/boxui.envfile
DynamicUser=yes
RuntimeDirectory=boxui
StateDirectory=boxui
CacheDirectory=boxui
LogsDirectory=boxui
RuntimeDirectoryMode=0755
Restart=on-failure

[Install]
WantedBy=default.target
"""

boxui_systemd_socket_template = """
[Unit]
Description=BoxUI is the Management Interface for IUVIA
Documentation=https://iuvia.io/support
Requires=network.target

[Socket]
ListenStream=/run/iuvia/boxui/http.sock
SocketUser=www-data

[Install]
WantedBy=default.target
"""

ic_systemd_service_template = """
[Unit]
Description=iuvia.control is the system service that manages \
    privileged operations between the Operating System layer \
    and the IUVIA Platform
Documentation=https://iuvia.io/support

[Service]
Type=notify
NotifyAccess=all
WorkingDirectory=/tmp
ExecStart=python3 -m iuvia.control.server --init-nginx
User=root
Restart=on-failure

[Install]
WantedBy=default.target
"""

ic_systemd_socket_template = """
[Unit]
Description=iuvia.control is the system service that manages \
    privileged operations between the Operating System layer \
    and the IUVIA Platform
Documentation=https://iuvia.io/support

[Socket]
ListenStream={control_sock}
SocketUser=root
SocketMode=0666


[Install]
WantedBy=default.target
"""

systemd_files = {
    "boxui.service": boxui_systemd_service_template,
    "boxui.socket": boxui_systemd_socket_template,
    "iuvia.control.service": ic_systemd_service_template,
    "iuvia.control.socket": ic_systemd_socket_template,
}


@click.group()
def main():
    if os.geteuid() != 0:
        click.echo(
            "Please run this program as root, or you will not be able to handle systemd files."
        )
        raise Exception("ENOPERM")


@main.command()
@click.option(
    "--systemd-system-location",
    default="/etc/systemd/system",
    type=click.Path(
        exists=True,
        file_okay=False,
        dir_okay=True,
        writable=True,
        readable=True,
        resolve_path=True,
        allow_dash=False,
    ),
)
@click.option("--boxui-port", default=80, type=int)
@click.option("--boxui-migrate/--no-boxui-migrate", is_flag=True, default=False)
@click.option("--now/--no-now", is_flag=True, default=True)
def install(systemd_system_location, boxui_port, boxui_migrate, now):
    prefix = systemd_system_location

    click.echo("Systemd location: {}".format(systemd_system_location))

    formats = defaultdict(dict)
    formats.update(
        **{
            "boxui.service": {"port": str(boxui_port)},
            "boxui.socket": {"port": str(boxui_port)},
            "iuvia.control.service": {},
            "iuvia.control.socket": {
                "control_sock": iuvia.control.internal.CONTROL_SOCK_PATH
            },
        }
    )

    for filename, content in systemd_files.items():
        with open(os.path.join(prefix, filename), "w") as fh:
            fh.write(content.format(**formats[filename]))
    subprocess.check_call(["/usr/bin/systemctl", "daemon-reload"])  # noqa: S603

    if boxui_migrate:
        click.echo("Migrating boxui before starting up")
        subprocess.check_call(
            [
                "/usr/bin/systemd-run",
                "--property=EnvironmentFile=/etc/systemd/system/boxui.envfile",
                "-t",
                "python3",
                "/opt/boxui/manage.py",
                "migrate",
            ]
        )
    enable_cmds = [
        ["/usr/bin/systemctl", "enable", "boxui.socket"],
        ["/usr/bin/systemctl", "enable", "iuvia.control.socket"],
    ]

    if now:
        # Add --now to the enable commands
        for cmd in enable_cmds:
            last = cmd[-1]
            cmd[-1] = "--now"
            cmd.append(last)
    for cmd in enable_cmds:
        subprocess.check_call(cmd)  # noqa: S603
    click.echo("Boxui and iuvia.control sockets started successfully")


def remove(systemd_system_location):
    prefix = systemd_system_location
    for filename in systemd_files:
        try:
            os.unlink(os.path.join(prefix, filename))
        except Exception as exn:
            print("Got a problem while unlinking file: {}".format(filename), exn)
