BoxUI
=====

User interface webapp for the Box

:License: AGPLv3


Development environment
-----------------------

In order to set up a development environment, you will need to use Docker.

You can use `docker-compose up -d` to set up all necessary infrastructure.

Users are retrieved from LDAP, which is part of the Docker Compose deployment. However, the first initial superuser can be created through the Django CLI utility in order to speed up development. In order to do that, log into the container (with `docker-compose exec`) and run the `django-admin createsuperuser` command as usual.::

    docker-compose run --rm django python manage.py createsuperuser

In order to compile and bundle the Vue files you will need to set up the Vue building system, which is part of the Docker Compose deployment. Thus, no further instructions should be required. Take into consideration that Django is bound to port :8000 and Vue to :8080, so no other apps should be using those ports while developing BoxUI. You can change those ports, but you will have to refer to the Docker Compose file and the different settigns for each service.


Deployment environment
----------------------

In order to correctly deploy BoxUI, a versioned wheel should be created. Until we reach the point of maturity of releasing wheels in a public server, the following can be done.

First, create the appropriate Django bundle for Vue files. That entails running a node environment in order to sue vue-cli. The most straightforward way of doing that is via docker-compose, by issuing the following command: ::

    docker-compose run --rm -v $PWD/staticfiles:/usr/local/app/dist -v $PWD/frontend/.bundletracker:/usr/local/app/.bundletracker vue npm run build

This should create the required files and put them into Django's staticfiles. Now you can package the wheel and/or upload the resulting files/folder into the destination.


Settings
--------

Moved to settings_.

.. _settings: http://cookiecutter-django.readthedocs.io/en/latest/settings.html

Basic Commands
--------------

Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^

* To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

* To create an **superuser account**, use this command::

    $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.

Type checks
^^^^^^^^^^^

Running type checks with mypy:

::

  $ mypy boxui

Test coverage
^^^^^^^^^^^^^

To run the tests, check your test coverage, and generate an HTML coverage report::

    $ coverage run -m pytest
    $ coverage html
    $ open htmlcov/index.html

Running tests with py.test
~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ pytest


Celery
^^^^^^

This app comes with Celery.

To run a celery worker:

.. code-block:: bash

    cd boxui
    celery -A config.celery_app worker -l info

Please note: For Celery's import magic to work, it is important *where* the celery commands are run. If you are in the same folder with *manage.py*, you should be right.




Deployment
----------

The following details how to deploy this application.



