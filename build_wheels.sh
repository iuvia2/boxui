#!/bin/bash

docker-compose run --rm --entrypoint=pip django wheel -w build/wheels/py37 '.[dev]'
