const axios = {
  get: () => new Promise(res => res({
    data: { title: 'Mock GET method', token: 'mytoken', username: 'myusername' }
  })),
  post: () => new Promise(res => res({
    data: { title: 'Mock POST method', token: 'mytoken', username: 'myusername' }
  })),
  create: jest.fn(function () {
    return this;
  }),
}

export default axios
