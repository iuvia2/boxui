export default function PieChart(series, labels) {
  this.series = series
  this.chartOptions = {
    labels,
    responsive: [{
      breakpoint: 480,
      options: {
        legend: {
          position: 'bottom',
        },
      },
    }],
    legend: {
      position: 'right',
    },
  }
}
