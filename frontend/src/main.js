import Vue from 'vue'
import Buefy from 'buefy'
import moment from 'moment'
import axios from './axios'
import router from './router'
import store from './store'
import settings from './settings'
import i18n from './i18n'
import App from './App.vue'

import './styles/styles.scss'

window.axios = axios
window.moment = moment

/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
axios.interceptors.request.use(config => {
  config.headers.common['Accept-Language'] = process.env.VUE_APP_I18N_LOCALE || process.env.VUE_APP_I18N_FALLBACK_LOCALE
  config.headers.common.Authorization = `Bearer ${store.getters.token}`
  return config
})

axios.interceptors.response.use(response => {
  store.commit('clearErrors')
  return response
}, error => {
  const headers = error.response.headers
  if (headers['content-type'].indexOf('text') > -1) {
    error.response.data = [i18n.t(settings.ERRORS.SERVER_ERROR)]
  }
  return Promise.reject(error)
})
/* eslint-enable no-param-reassign */
/* eslint-enable prefer-destructuring */

Vue.config.productionTip = false
Vue.use(Buefy)

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app')
