import BoxDashboard from '@/views/Dashboard.vue'
import BoxHome from '@/views/Home.vue'
import BoxLaunchers from '@/views/Launchers.vue'
import BoxLogin from '@/views/Login.vue'
import BoxMarketplace from '@/views/Marketplace.vue'
import BoxSettings from '@/views/Settings.vue'

import BoxAppManagerDetail from '@/views/settings/AppManagerDetail.vue'
import BoxAppManager from '@/views/settings/AppManager.vue'
import BoxDeviceManager from '@/views/settings/DeviceManager.vue'
import BoxSessionManager from '@/views/settings/SessionManager.vue'
import BoxSubscriptionManager from '@/views/settings/SubscriptionManager.vue'
import BoxUserManager from '@/views/settings/UserManager.vue'

import BoxUserProfileManager from '@/views/settings/users/UserProfileManager.vue'
import BoxUserProfileNew from '@/views/settings/users/UserProfileNew.vue'

export default {
  BoxAppManagerDetail,
  BoxAppManager,
  BoxDashboard,
  BoxDeviceManager,
  BoxHome,
  BoxLaunchers,
  BoxLogin,
  BoxMarketplace,
  BoxSessionManager,
  BoxSettings,
  BoxSubscriptionManager,
  BoxUserManager,
  BoxUserProfileManager,
  BoxUserProfileNew,
}
