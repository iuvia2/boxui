import settings from './settings'

export default {
  transformErrorMsg(dict) {
    // Transform typical error message structures to an array of error messages with key and value E.g.
    const errorArr = []
    Object.keys(dict).forEach(key => {
      errorArr.push({
        key,
        value: dict[key]
      })
    })
    return errorArr
  },
  formatString(template, params) {
    let str = template
    if (params) {
      Object.keys(params).forEach(prop => {
        str = str.replace(new RegExp(`{\\s*${prop}\\s*}`), params[prop])
      })
    }
    return str
  },
  pathJoin(parts) {
    const separator = '/'
    const newParts = parts.map((part, index) => {
      let p = part
      if (index) {
        p = part.replace(new RegExp(`^${separator}`), '')
      }
      if (index !== parts.length - 1) {
        p = part.replace(new RegExp(`${separator}$`), '')
      }
      return p
    })
    return newParts.join(separator)
  },
  deviceIcon(device) {
    switch (device) {
      case 'mobile':
        return settings.DEVICE_ICONS.MOBILE
      case 'tablet':
        return settings.DEVICE_ICONS.TABLET
      default:
        return settings.DEVICE_ICONS.DESKTOP
    }
  },
}
