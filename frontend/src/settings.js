export default {
  URLS: {
    API: {
      BASE: `${window.location.protocol}//${window.location.host}/api`,
      USERS: '/users/',
      USER: '/users/{ username }/',
      LOGIN: '/users/login/',
      LOGOUT: '/users/logout/',
      LOGOUT_ALL: '/users/logout/all/',
      WHOAMI: '/users/me/',
      SESSIONS: '/users/sessions/',
      CLOSE_SESSION: '/users/sessions/',
      INSTALLED_APPS: '/appimages/_/platformapps/',
      MARKETED_APPS: '/appimages/_/platformapps/', // TODO: change for real one
      APP_DETAILS: '/appimages/{ appid }/',
      APP_INSTALL: '/appimages/{ appid }/install/',
      APP_UNINSTALL: '/appimages/{ appid }/uninstall/',
      APP_ICON: '/appimages/{ appid }/icon/{ size }/',
      LAUNCHERS: '/appimages/_/launchers/',
      LAUNCHER_ICON: '/appimages/_/launchers/{ launcherid }/icon/{ size }/',
    },
    APP: {
      BASE: `${window.location.protocol}//${window.location.host}/`,
    },
  },
  TIMEOUT: 100000,
  PROJECT_TITLE: 'BoxUI, a web interface for iuvia.io',
  DEVICE_ICONS: {
    MOBILE: 'cellphone',
    TABLET: 'tablet-ipad',
    DESKTOP: 'monitor',
  },
  PREFERRED_ICON_SIZE: 128,
  ERRORS: {
    SERVER_ERROR: 'Some server error happened. Please try again later. If the error persists contact your administrator.',
    NOT_FOUND_ERROR: 'Some server error happened. Please try again later. If the error persists contact your administrator.',
  },
  INSTALLED_STATE: {
    0: 'uninstalled',
    1: 'installed',
    2: 'install_pending',
    4: 'install_failed',
    7: 'installing',
    32: 'uninstalling',
    64: 'uninstall_failed',
    23: 'updating',
  },
}
