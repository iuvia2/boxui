import store from './store'
import i18n from './i18n'

export default {
  isAuthenticated(to, from, next) {
    if (store.getters.authenticated) {
      if (store.getters.user) {
        next()
      } else {
        store.dispatch('whoami').then(() => {
          next()
        }).catch(err => {
          console.error(err)
          const errors = [i18n.t('Token not valid')]
          store.commit('setErrors', { data: errors })
          store.commit('clearSession')
          if (from.name !== 'login') {
            next({ name: 'login', query: { redirect: to.path } })
          }
        })
      }
    } else {
      next({ name: 'login', query: { redirect: to.path } })
    }
  },
  redirectHome(to, from, next) {
    if (store.getters.authenticated) {
      next('/')
    } else {
      next()
    }
  },
  setTitleAndMeta(to, from, next) {
    // FInd closest route with a title.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title)

    // Find the nearest route with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags)

    // If a route with a title was found, set the document (page) title to that value.
    if (nearestWithTitle) {
      document.title = nearestWithTitle.meta.title
    }

    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el))

    // Skip rendering meta tags if there are none.
    if (nearestWithMeta) {
      // Turn the meta tag definitions into actual elements in the head.
      nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta')
        Object.keys(tagDef).forEach(key => {
          tag.setAttribute(key, tagDef[key])
        })
        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '')
        return tag
      }).forEach(tag => document.head.appendChild(tag))
    }
    next()
  },
  clearErrors(to, from, next) {
    store.commit('clearErrors')
    next()
  },
}
