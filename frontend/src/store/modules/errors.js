// import utils from '../../utils'

const state = {
  errors: {},
}

const getters = {
  errors: s => Object.values(s.errors),
  hasErrors: s => Object.keys(s.errors).length > 0,
  error: s => key => {
    let value = s.errors
    key.split('.').some(k => {
      if (Object.prototype.hasOwnProperty.call(value, k)) {
        value = value[k]
      } else {
        value = []
        return
      }
    })
    return value.toString()
    // If we go back to one level form fields, just do:
    // (s.errors.hasOwnProperty(key) ? s.errors[key] : null)
  },
  hasError: s => key => {
    let value = s.errors
    key.split('.').some(k => {
      if (Object.prototype.hasOwnProperty.call(value, k)) {
        value = value[k]
      } else {
        value = false
        return
      }
    })
    return value !== false
    // If we go back to one level form fields, just do:
    // return s.errors.hasOwnProperty(parts[0])
  },
}

const mutations = {
  setErrors(s, errorResponse) {
    s.errors = errorResponse.data
  },
  clearErrors(s) {
    s.errors = { }
  },
}

const actions = { }

export default {
  state,
  getters,
  actions,
  mutations,
}
