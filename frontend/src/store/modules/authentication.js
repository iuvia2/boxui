import api from '../../services/authentication'

const state = {
  authenticated: false,
  token: null,
  user: null,
}

const getters = {
  authenticated: s => s.authenticated,
  token: s => s.token,
  user: s => s.user,
  username: s => (s.user ? s.user.uid : null),
}

const mutations = {
  setToken(s, token) {
    s.token = token || null
    s.authenticated = !!token
  },
  setUser(s, user) {
    s.user = user
  },
  clearSession(s) {
    s.user = null
    s.token = null
    s.authenticated = false
  },
}

const actions = {
  authenticate({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api.authenticate(payload.username, payload.password).then(response => {
        commit('setToken', response.data.token)
        resolve(response.data.token)
      }).catch(err => {
        commit('setToken', null)
        commit('setUser', null)
        reject(err)
      })
    })
  },
  whoami({ commit }) {
    return new Promise((resolve, reject) => {
      api.whoami().then(response => {
        commit('setUser', response.data)
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },
  logout({ commit }) {
    return new Promise((resolve, reject) => {
      api.logout().then(() => {
        commit('setToken', null)
        commit('setUser', null)
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },
  logoutAll({ commit }) {
    return new Promise((resolve, reject) => {
      api.logoutAll().then(() => {
        commit('setToken', null)
        commit('setUser', null)
        resolve()
      }).catch(err => {
        reject(err)
      })
    })
  },
}

export default {
  state,
  getters,
  actions,
  mutations,
}
