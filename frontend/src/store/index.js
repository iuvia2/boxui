import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import authentication from './modules/authentication'
import errors from './modules/errors'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    authentication,
    errors,
  },
  strict: debug,
  plugins: [createPersistedState({
    key: 'boxuistate',
    paths: ['authentication.token', 'authentication.authenticated'],
  })],
})
