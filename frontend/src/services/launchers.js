import axios from '../axios'
import settings from '../settings'

// TODO: favorite and latest launchers,
// replace with actual API call
export default {
  getLaunchers() {
    return axios.get(settings.URLS.API.LAUNCHERS)
  },
  getFavoriteLaunchers() {
    return axios.get(settings.URLS.API.LAUNCHERS)
  },
  getLatestLaunchers() {
    return axios.get(settings.URLS.API.LAUNCHERS)
  },
}
