import settings from '../settings'
import axios from '../axios'
import utils from '../utils'

export default {
  getAllUsers() {
    return axios.get(settings.URLS.API.USERS)
  },
  createNewUser(payload) {
    return axios.post(settings.URLS.API.USERS, payload)
  },
  getUser(username) {
    const url = utils.formatString(settings.URLS.API.USER, { username })
    return axios.get(url)
  },
  deleteUser(username) {
    const url = utils.formatString(settings.URLS.API.USER, { username })
    return axios.delete(url)
  },
  updateUser(username, payload) {
    const url = utils.formatString(settings.URLS.API.USER, { username })
    return axios.patch(url, payload)
  },
}
