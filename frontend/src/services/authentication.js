import settings from '../settings'
import axios from '../axios'

export default {
  authenticate(username, password) {
    return axios.post(settings.URLS.API.LOGIN, {}, {
      auth: {
        username,
        password,
      },
    })
  },
  whoami() {
    return axios.get(settings.URLS.API.WHOAMI)
  },
  logout() {
    return axios.post(settings.URLS.API.LOGOUT)
  },
  logoutAll() {
    return axios.post(settings.URLS.API.LOGOUT_ALL)
  },
  getSessions() {
    return axios.get(settings.URLS.API.SESSIONS)
  },
  closeSession(token) {
    return axios.delete(`${settings.URLS.API.CLOSE_SESSION}${token}`)
  },
}
