// import axios from '../axios'
// import settings from '../settings'

import DASHBOARD from './mock-dashboard.json'

// TODO: Mock for GET dashboard API,
// replace with actual API call
export default {
  getData() {
    return new Promise(resolve => {
      resolve(DASHBOARD)
    })
  },
}
