import axios from '../axios'
import settings from '../settings'
import utils from '../utils'

export default {
  getUrl(url) {
     return axios.get(url)
  },
  getMarketedApps() {
    return axios.get(settings.URLS.API.MARKETED_APPS)
  },
  getInstalledApps() {
    return axios.get(settings.URLS.API.INSTALLED_APPS)
  },
  getAppDetails(appid) {
    const url = utils.formatString(settings.URLS.API.APP_DETAILS, { appid })
    return axios.get(url)
  },
  installApp(appid) {
    const url = utils.formatString(settings.URLS.API.APP_INSTALL, { appid })
    return axios.get(url)
  },
  uninstallApp(appid) {
    const url = utils.formatString(settings.URLS.API.APP_UNINSTALL, { appid })
    return axios.get(url)
  },
}
