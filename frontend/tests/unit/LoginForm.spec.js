import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import LoginForm from '@/components/authentication/LoginForm.vue'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('LoginForm', () => {
  let store
  let actions
  const getters = {
    hasErrors: () => false,
  }
  const mocks = {
    $t: msg => msg,
  }

  beforeEach(() => {
    actions = {
      authenticate: jest.fn(),
    }
    store = new Vuex.Store({
      actions,
      state: {},
      getters,
      mutations: {
        setErrors: jest.fn(),
      },
    })
  })

  it('renders an empty login form on init', () => {
    const wrapper = shallowMount(LoginForm, {
      localVue,
      store,
      mocks,
    })

    expect(wrapper.element).toMatchSnapshot()
  })

  describe('when the user is login in', () => {
    it('login button is disabled', () => {
      const wrapper = shallowMount(LoginForm, {
        localVue,
        store,
        mocks,
      })

      const loginForm = wrapper.find('form')
      loginForm.trigger('submit')
      wrapper.vm.$nextTick()

      const loginBtn = wrapper.find('box-button-stub')
      expect(loginBtn.props('disabled')).toBe(true)
    })
  })
})
