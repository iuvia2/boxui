import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import authentication from '@/store/modules/authentication.js'

describe('Store authentication actions', () => {
  let store
  let mockMutation

  beforeEach(() => {
    mockMutation = jest.fn()

    store = new Vuex.Store({
      state: {},
      mutations: {
        setToken: mockMutation,
        setUser: mockMutation,
      },
      actions: {
        authenticate: authentication.actions.authenticate,
        whoami: authentication.actions.whoami,
        logout: authentication.actions.logout,
      },
    })
  })

  it('tests authenticate action and using a mock mutation', () => {
    return store.dispatch('authenticate', { username: 'myusername', password: 'mypassword' })
      .then(response => {
        expect(mockMutation.mock.calls[0][1])
          .toEqual('mytoken')
        expect(mockMutation.mock.calls).toHaveLength(1)
      })
  })

  it('tests whoami action using mock mutations', () => {
    return store.dispatch('whoami')
      .then(response => {
        expect(mockMutation.mock.calls).toHaveLength(1)
      })
  })
})

describe('Store authentication mutations', () => {
  const state = {
    authenticated: false,
    token: null,
    user: null,
  }

  it('tests authentication setToken mutation', () => {
    const token = 'mytoken'
    authentication.mutations.setToken(state, token)
    expect(state.token).toEqual(token)
    expect(state.authenticated).toBe(true)
    authentication.mutations.setToken(state, null)
    expect(state.token).toEqual(null)
    expect(state.authenticated).toBe(false)
  })

  it('tests authentication setUser mutation', () => {
    const user = {
      username: 'myusername',
      email: 'my@email.com'
    }
    authentication.mutations.setUser(state, user)
    expect(state.user).toEqual(user)
  })
})
