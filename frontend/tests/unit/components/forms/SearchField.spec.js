import { mount, createLocalVue } from '@vue/test-utils'
import Buefy from 'buefy'
import BoxSearch from '@/components/forms/SearchField.vue'

const localVue = createLocalVue()

localVue.use(Buefy)

describe('SearchField', () => {
  it('renders a placeholder when passed', () => {
    const placeholder = 'Search an app'

    const wrapper = mount(BoxSearch, {
      localVue,
      propsData: { placeholder },
    })

    expect(wrapper.html()).toContain(`placeholder="${placeholder}"`)
  })

  it('do not render a prefix icon when passed', () => {
    const wrapper = mount(BoxSearch, {
      localVue,
    })

    expect(wrapper.html()).not.toContain('has-icons-left')
  })

  it('renders a prefix icon when passed', () => {
    const wrapper = mount(BoxSearch, {
      localVue,
      propsData: { hasPrefixIcon: true },
    })

    expect(wrapper.html()).toContain('chevron-down')
  })

  it('binds value property to input value', () => {
    const wrapper = mount(BoxSearch, {
      localVue,
      propsData: { value: 'some search text' },
    })

    expect(wrapper.find('input').element.value).toBe('some search text')
  })

  it('emits event when user performs search clicking the search button', async () => {
    const wrapper = mount(BoxSearch, {
      localVue,
    })

    wrapper.setData({ inputVal: 'some search' })
    wrapper.find('button').trigger('click')

    await wrapper.vm.$nextTick()

    expect(wrapper.emitted().submit[0]).toContain('some search')
  })

  xit('emits event when user performs search pressing enter', async () => {
    // TODO I couldnt trigger an event on the input element, so I cannot test event management
    // TODO It has been tested manually in browser
    // const wrapper = mount(BoxSearch, {
    //   localVue,
    // })
    //
    // wrapper.setData({ inputVal: 'some search' })
    // wrapper.find('input').element.dispatchEvent(new KeyboardEvent('keypress', { keyCode: 13 }))
    // wrapper.find('input').trigger('keypress.native', { keyCode: 13 })
    //
    // await wrapper.vm.$nextTick()
    //
    // expect(wrapper.emitted().submit[0]).toContain('some search')
  })
})
