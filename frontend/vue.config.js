const BundleTracker = require("webpack-bundle-tracker");
const path = require("path");

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/static' : 'http://0.0.0.0:8081/',
  outputDir: './dist/',

  chainWebpack: config => {
    config.optimization
      .splitChunks(false)

    config
      .plugin('BundleTracker')
      .use(BundleTracker, [{filename: './.bundletracker/webpack-stats.json'}])

    config.resolve.alias
      .set('__STATIC__', 'static')

    config.devServer
      .public('http://0.0.0.0:8081')
      .host('0.0.0.0')
      .port(8081)
      .hotOnly(true)
      .watchOptions({poll: 1000})
      .https(false)
      .headers({"Access-Control-Allow-Origin": ["\*"]})
      .progress(false)
      .stats('errors-only')
      .quiet(true)
      .public('boxui.local.dev.net.iuvia.io:8081')
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  },

  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/styles/_types.scss";
          @import "@/styles/_palette.scss";
          @import "@/styles/_variables.scss";
          @import "@/styles/_mixins.scss";
        `
      }
    }
  }
};
