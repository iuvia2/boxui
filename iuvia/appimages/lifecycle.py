from iuvia.appimages import AppImageOps
from iuvia.control.client import API as ICAPI


class PrivilegedLifecycleManager(object):
    def install(self):
        pass

    @property
    def socket_path(self) -> str:
        raise NotImplementedError("")

    def uninstall(self):
        pass

    def is_active(self) -> bool:
        return False


class LifecycleManager(object):
    """
    AppImage Lifecycle Manager class.

    The Lifecycle manager helps communicate through iuvia.control to
    install and remove AppImages.  This should allow in the future to
    change the system interface (whatever we want to use, upstart,
    systemd, or even Docker containers) without changing the user
    code.
    """
    def __init__(self, ops: AppImageOps):
        self.ops = ops

    @staticmethod
    def create(ops: AppImageOps) -> "LifecycleManager":
        return LifecycleManager(ops)

    @property
    def socket_path(self) -> str:
        return self.privileged.socket_path

    @property
    def privileged(self) -> PrivilegedLifecycleManager:
        from iuvia.appimages.internal.systemd import PrivilegedSDLifecycleManager
        return PrivilegedSDLifecycleManager(self.ops)

    def install(self, subdomain):
        api = ICAPI()
        api.install_appimage(self.ops.path)
        api.register_service(
            name=subdomain,
            unix_socket_http=self.socket_path,
        )

    def uninstall(self, subdomain):
        api = ICAPI()
        api.uninstall_appimage(self.ops.path)
        api.unregister_service(subdomain)
