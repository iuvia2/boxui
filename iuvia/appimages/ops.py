import contextlib
import logging
import shutil
import subprocess
from os import environ, path, getcwd, devnull
from iuvia.proto import appimage_pb2
import typing

__all__ = ["AppImageOps"]


_LOGGER = logging.getLogger(__name__)
_EXTRACTION_DIR = "squashfs-root"
_ENTRYPOINT = "AppRun"
_FNULL = open(devnull, "w")

DEFAULT_RUNNER = subprocess.check_output


class AppImageOps:
    """
    Perform low-level operations with AppImages, including running
    the different IUVIA Platform commands.

    By personalizing the runner, you can run the applications in a
    different context or with a system manager, for handling
    permissions, capabilities, cgroups and other information, which
    subprocess.check_output can't handle.
    """
    runner = None

    def __init__(self, appimage_path: str, extract_before_run: bool = None):
        self.path = appimage_path
        if extract_before_run is None:
            self.extract = environ.get("USE_DOCKER") == "yes"
        else:
            self.extract = bool(extract_before_run)

    @contextlib.contextmanager
    def use_runner(self, runner):
        """
        Use a different runner than subprocess.check_output.

        By setting a runner, you can run the AppImageOps operations
        within a setup that handles permissions, capabilities,
        cgroups, environment variables and other nuances that
        subprocess.check_output does not handle for you.

        This is primarily useful to run the AppImage in a restricted
        environment, when the process calling handling the AppImageOps
        is launched by a privileged user.
        """
        prev_runner = self.runner
        try:
            self.runner = runner
            yield self
        finally:
            self.runner = prev_runner

    def run(self, *args) -> bytes:
        """
        Friendly high-level interface for running AppImages.
        This method passes all args to the underlying AppImage.

        If required (via USE_DOCKER), it extracts the AppImage
        before running it (cleaning afterwards)
        """
        if not self.runner:
            self.runner = DEFAULT_RUNNER
        if self.extract:
            return self._extract_and_run_appimage(args)
        else:
            return self._run_appimage(args)

    def _run_appimage(self, args: typing.Collection[str] = ()):
        args = list(args)
        try:
            return self.runner([self.path] + args)  # noqa: S603
        except Exception as e:
            _LOGGER.error("Some error happened while collecting AppImage information.")
            raise e

    def _extract_and_run_appimage(self, args: typing.Collection[str] = ()):
        args = list(args)
        squash_dir = path.join(getcwd(), _EXTRACTION_DIR)
        apprun_path = path.join(squash_dir, _ENTRYPOINT)
        try:
            subprocess.call(  # noqa: S603
                [self.path, "--appimage-extract"],
                stdout=_FNULL,
                stderr=subprocess.STDOUT,
            )
            return self.runner([apprun_path] + args)  # noqa: S603
        except Exception as e:
            _LOGGER.error("Some error happened while collecting AppImage information.")
            raise e
        finally:
            shutil.rmtree(squash_dir, ignore_errors=True)

    def get_metadata_raw(self):
        return self.run("--iuvia-platform", "metadata")

    def get_metadata_pb(self) -> appimage_pb2.AppImage:
        # TODO: Retrieve signature bits and throw here if invalid developer/signature
        raw = self.get_metadata_raw()
        if not raw:
            return None
        obj = appimage_pb2.AppImage()
        obj.ParseFromString(raw)
        return obj

    def get_icon(self, icon_path):
        return self.run("--iuvia-platform", "metadata", "-i", icon_path)

    def get_signature(self):
        return self._run_appimage(["--appimage-signature"])

    def run_install(self):
        return self.run("--iuvia-platform", "install")

    def run_uninstall(self):
        return self.run("--iuvia-platform", "uninstall")

    def run_service(self):
        return self.run("--iuvia-platform", "run")
