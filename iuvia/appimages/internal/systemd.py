import collections.abc
import functools
import json
import os
import typing
from collections import defaultdict
from functools import lru_cache

from iuvia.appimages import AppImageOps
from iuvia.appimages.lifecycle import PrivilegedLifecycleManager
from iuvia.control.utils import systemctl

from typing import Mapping, Any, Union, Sequence, Dict, Generator, List

__all__ = ["SDUnit", "UnitSet"]

DEFAULT_SYSTEMD_RUNTIME_LOCATION = "/run/systemd/system"


def _update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = _update(d.get(k, defaultdict(defaultdict)), v)
        else:
            d[k] = v
    return d


class SDUnit(object):
    """
    Systemd-unit.

    This class encompasses the name and type of unit, as well as the
    text that should go into the respective file.
    """

    @classmethod
    def ensure(cls, self: Union[str, "SDUnit"]) -> "SDUnit":
        if isinstance(self, str):
            return SDUnit("unknown", self)
        else:
            return self

    def __init__(self, name: str, type: str, text: str = None):
        self.name = name
        self.type = type
        self.text = text

    def __repr__(self):
        return "<SDUnit {}.{}>".format(self.name, self.type)

    def __str__(self):
        return "{}.{}".format(self.name, self.type)

    def path(self, prefix):
        return os.path.join(prefix, str(self))

    def ensure_fs(self, systemd_unit_path):
        with open(self.path(systemd_unit_path), "w") as f:
            f.write(self.text)

    def unlink_fs(self, systemd_unit_path, ignore_unexisting=True):
        try:
            os.remove(self.path(systemd_unit_path))
        except FileNotFoundError as e:
            if not ignore_unexisting:
                raise e


class UnitSet(object):
    """
    A set of systemd units for a given IUVIA AppImage.

    IUVIA AppImages usually use a combination of a systemd.socket
    plus a systemd.service, which is then spun up when traffic hits
    the socket.

    A UnitSet can write all units into runtime files
    at /run/systemd/system by using the write_all and unlink_all
    functions.
    """

    unit_sections = {
        "service": ["Unit", "Service", "Install"],
        "socket": ["Unit", "Socket", "Install"],
    }

    default_props = {"Service": {"DynamicUser": "yes"}}

    def __init__(
        self,
        props: Mapping[str, Mapping[str, Any]],
        name: str,
        socket_type: Union[None, str] = "http",
        units: Sequence[str] = ("socket", "service"),
    ):
        self.props: Dict[str, Dict[str, Any]] = defaultdict(defaultdict)
        _update(self.props, self.default_props)
        _update(self.props, props)

        self.name = name
        self._units = units

        if socket_type == "http":
            self.props["Socket"]["ListenStream"] = "/run/http/{}.sock".format(self.name)
            self.props["Install"]["WantedBy"] = "default.target"
        else:
            raise NotImplementedError(
                "Only units with http sockets can be deployed in this version."
            )

    def to_json(self) -> str:
        return json.dumps(
            {"name": self.name, "props": self.props, "units": self._units}
        )

    @staticmethod
    def from_json(text: str) -> "UnitSet":
        obj = json.loads(text)
        props = UnitSet._defaultdict_from_dict(obj["props"])
        return UnitSet(name=obj["name"], units=obj["units"], props=props)

    @staticmethod
    def _defaultdict_from_dict(props: dict) -> Dict[str, Dict[str, Any]]:
        for k, v in props.items():
            if isinstance(v, dict):
                props[k] = UnitSet._defaultdict_from_dict(v)
        dd: Dict[str, Dict[str, Any]] = defaultdict(defaultdict)
        _update(dd, props)
        return dd

    @property
    def units(self) -> List[SDUnit]:
        return [SDUnit(self.name, unit, text=self.text(unit)) for unit in self._units]

    @property
    def socket_path(self) -> str:
        return (
            self.props["Socket"]["ListenStream"]
            or self.props["Socket"]["ListenSequentialPacket"]
            or self.props["Socket"]["ListenDatagram"]
        )

    def text(self, unit="service") -> str:
        return str.join("\n", self._as_list(unit=unit))

    def _as_list(self, unit: Union[str, SDUnit] = "service") -> Generator:
        unit = SDUnit.ensure(unit)
        for section in self.unit_sections[unit.type]:
            yield "[{}]".format(section)
            for prop, val in self.props[section].items():
                yield "{key}={value}".format(key=prop, value=val)
            yield ""

    def write_all(self, systemd_unit_path: str = DEFAULT_SYSTEMD_RUNTIME_LOCATION):
        for unit in self.units:
            unit.ensure_fs(systemd_unit_path)


def privileged(f):
    """
    Decorator ward to ensure privileged operations are only run
    by privileged processes.
    """

    def check(*args, **kwargs):
        if os.geteuid() != 0:
            raise AssertionError(
                "Tried to access the Privileged Lifecycle Manager from a non-privileged process. Abort."
            )
        return f(*args, **kwargs)

    return functools.wraps(f)(check)


class PrivilegedSDLifecycleManager(PrivilegedLifecycleManager):
    """
    For operations that need to be privileged. These should be run from
    a daemon that has access to the privileged system manager and can
    configure it.
    """

    transient_units_prefix = "io.iuvia."
    runtime_unit_path = "/run/systemd/system/"

    def __init__(self, appimage: AppImageOps):
        self.ops = appimage

    @property
    @lru_cache()
    def codename(self) -> str:
        import subprocess

        with self.ops.use_runner(subprocess.check_output):
            meta = self.ops.get_metadata_pb()
        return meta.codename

    def _systemd_run(self, args: typing.Collection[str]):
        """
        Runner that can run ops operations inside systemd-run units
        """
        import subprocess

        cmd = [
            f.format(codename=self.codename)
            for f in [
                "/usr/bin/systemd-run",
                "--pipe",
                "--same-dir",
                "--wait",
                "--collect",
                "--service-type=exec",
                "--property=DynamicUser=yes",
                "--property=StateDirectory=iuvia/apps/{codename}",
                "--property=CacheDirectory=iuvia/apps/{codename}",
                "--property=LogsDirectory=iuvia/apps/{codename}",
                "--property=RuntimeDirectory=iuvia/apps/{codename}",
                "--property=StateDirectoryMode=0700",
                "--property=CacheDirectoryMode=0700",
            ]
        ] + args

        print("CMD: ", cmd)
        return subprocess.check_output(args=cmd, bufsize=None)

    @privileged
    def install(self):
        with self.ops.use_runner(self._systemd_run):
            self.ops.run_install()
        unit_set = self._generate_systemd_units()
        unit_set.write_all(self.runtime_unit_path)
        systemctl.daemon_reload()
        systemctl.start(str(unit_set.units[0]))

    @privileged
    def uninstall(self):
        for unit in self._generate_systemd_units().units:
            systemctl.stop(str(unit))
            try:
                unit.unlink_fs(self.runtime_unit_path)
            except FileNotFoundError:
                pass
        with self.ops.use_runner(self._systemd_run):
            self.ops.run_uninstall()
        systemctl.daemon_reload()

    @property
    @privileged
    def is_active(self) -> bool:
        unit_set = self._generate_systemd_units()
        socket_unit = str(unit_set.units[0])
        return systemctl.is_active(socket_unit)

    @property
    def socket_path(self):
        if 'nextcloud' in self.codename:
            return '127.0.0.1:8081'
        return self._generate_systemd_units().socket_path

    @lru_cache()
    def _generate_systemd_units(self):
        meta = self.ops.get_metadata_pb()
        safepath = self.ops.path.replace('"', '"')
        if " " in safepath:
            safepath = '"{}"'.format(safepath)
        codename = meta.codename
        units = ["socket", "service"]
        # FIXME: Prevent this hotfix
        if meta.codename == "io.iuvia.demo.nextcloud":
            units = ["service"]

        app = UnitSet(
            name=self.transient_units_prefix + codename,
            props={
                "Unit": {
                    "Description": "An autogenerated unit for {} v{}".format(
                        codename, meta.version
                    )
                },
                "Service": {
                    "ExecStart": self.ops.path + " --iuvia-platform run",
                    "DynamicUser": "yes",
                    "StateDirectory": "iuvia/apps/" + codename,
                    "CacheDirectory": "iuvia/apps/" + codename,
                    "LogsDirectory": "iuvia/apps/" + codename,
                    "RuntimeDirectory": "iuvia/apps/" + codename,
                    "StateDirectoryMode": "0700",
                    "CacheDirectoryMode": "0700",
                },
                "Socket": {"SocketUser": "www-data"},
            },
            units=units,
        )

        return app
