import asyncio
from asyncio.streams import StreamReader
import json

import click
from iuvia.control.internal import CONTROL_SOCK_PATH as _CONTROL_SOCK_PATH


class API:
    def __init__(self, loop=None):
        self.loop = loop

    @staticmethod
    async def expect_signature(reader: StreamReader):
        first = await reader.readline()
        proto_version = await reader.readline()
        if first != b"iuvia.control\n" or proto_version != b"version 1\n":
            raise Exception("Unexpected protocol from iuvia.control connection")

    async def _aread_all_after_write(self, the_bytes: bytes):
        reader, writer = await asyncio.streams.open_unix_connection(
            path=_CONTROL_SOCK_PATH, loop=self.loop,
        )
        await self.expect_signature(reader)
        writer.write(the_bytes)
        writer.write_eof()
        await writer.drain()
        response = await reader.read()
        writer.close()
        return response

    def _read_all_after_write(self, the_bytes: bytes):
        return asyncio.run(self._aread_all_after_write(the_bytes))

    def get_version(self):
        return self._read_all_after_write(b"version\n")

    def register_service(
        self, name=None, systemd_unit=None, unix_socket_http=None,
    ):
        json_msg = json.dumps(
            {
                "name": name,
                "systemd_unit": systemd_unit,
                "unix_socket_http": unix_socket_http,
            }
        )
        payload = json_msg.encode("utf-8")
        msg = "register_service {}\n".format(len(payload)).encode("utf-8") + payload
        return self._read_all_after_write(msg)

    def unregister_service(self, name=None):
        msg = "unregister_service {}\n".format(name).encode("utf-8")
        return self._read_all_after_write(msg)

    def install_appimage(self, path: str):
        msg = "install_appimage {}\n".format(path).encode("utf-8")
        response = self._read_all_after_write(msg)
        if b"INSTALL_OK\n" in response:
            return response
        elif b"ERR CODE" in response:
            raise RuntimeError(response.decode("utf-8"))
        else:
            raise RuntimeError("Unknown error")

    def uninstall_appimage(self, path: str):
        msg = "uninstall_appimage {}\n".format(path).encode("utf-8")
        response = self._read_all_after_write(msg)
        if b"UNINSTALL_OK\n" in response:
            return True
        elif b"ERR CODE" in response:
            raise RuntimeError(response.decode("utf-8"))
        else:
            raise RuntimeError("Unknown error")

    def update_appimage(self, old_path: str, new_path: str):
        # TODO: Implement updates
        raise NotImplementedError("Updates are not implemented yet")

    def debug_registered(self):
        return self._read_all_after_write(b"debug_registered\n")

    def debug_nginx_conf(self):
        return self._read_all_after_write(b"debug_nginx_conf\n")

    def check_noop(self):
        """
        Creates a connection and does a noop (bye)
        """
        return self._read_all_after_write(b"bye\n")


@click.command()
@click.option("--run-async", help="Run the mode with asyncio.run (only for internals)", default=False)
@click.argument("mode")
@click.argument("args", nargs=-1)
def cli(mode, run_async, args):
    click.echo("Using mode: {mode}".format(mode=mode,))

    api = API()

    if mode == "register_service_demo":
        click.echo(
            api.register_service(
                name="boxui",
                systemd_unit="/etc/systemd/system/boxui.socket",
                unix_socket_http="unix:/run/iuvia/boxui/http.sock",
            )
        )

    elif mode == "register_service_nextcloud":
        click.echo(
            api.register_service(
                name="nextcloud",
                systemd_unit="/run/systemd/system/nextcloud.service",
                unix_socket_http="127.0.0.1:8081",
            )
        )

    elif hasattr(api, mode):
        attr = getattr(api, mode)
        if callable(attr):
            if run_async:
                click.echo(asyncio.run(attr(*args)))
            else:
                click.echo(attr(*args))
        else:
            click.echo(attr)
    else:
        click.echo("Not understood")


if __name__ == "__main__":
    cli()
