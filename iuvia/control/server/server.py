# iuvia.control.server
# This file is part of iuvia.control

import asyncio
from asyncio.streams import StreamReader, StreamWriter
import json
import os
import pickle  # noqa: S403
import socket
import subprocess  # noqa: S404
import typing

import click
from iuvia.appimages import AppImageOps
from iuvia.control import __version__
from iuvia.control.utils import systemctl
from iuvia.appimages.lifecycle import LifecycleManager
from iuvia.control.server.jinjaparser import render_nginx_conf
from . import sd_glue as systemd

SERVER_CACHE_FILE = "/run/iuvia/control/cache.pickle"


def check_runtime():
    if os.geteuid() != 0:
        raise Exception("You need to run this as root.")


class ControlServer:
    transient_units_prefix = "io.iuvia."
    runtime_unit_path = "/run/systemd/system/"

    def __init__(self):
        self.services: typing.Dict[str, typing.Dict[str, typing.Any]] = {}
        self.domains = [
            "local.dev.net.iuvia.io",
        ]

    def register(
        self, name=None, systemd_unit=None, unix_socket_http=None, **kwargs,
    ):
        self.services[name] = {
            "name": name,
            "path": "/{}/".format(name),
            "default": 0,
            "backend": unix_socket_http,
            "systemd_unit": systemd_unit,
            "kwargs": kwargs,
        }

    def unregister(self, name):
        del self.services[name]

    async def install_appimage(self, appimage_path, occ):
        # Get metadata
        ops = AppImageOps(appimage_path)
        plm = LifecycleManager.create(ops).privileged
        try:
            plm.install()
            occ.writer.write(b"INSTALL_OK\n")
            occ.writer.write(
                "SOCKET_AT {}\n".format(plm.socket_path).encode("utf-8")
            )
        except subprocess.CalledProcessError as e:
            occ.writer.write(
                "ERR CODE {}\nON_CMD {}\n".format(str(e.returncode), e.cmd,).encode(
                    "utf-8"
                )
            )
            await occ.writer.drain()
        except FileNotFoundError:
            occ.writer.write(b"ERR FILE_NOT_FOUND\n")
            await occ.writer.drain()

    async def uninstall_appimage(self, appimage_path, occ):
        # Get metadata
        ops = AppImageOps(appimage_path)
        plm = LifecycleManager.create(ops).privileged
        try:
            plm.uninstall()
            occ.writer.write(b"UNINSTALL_OK\n")
        except subprocess.CalledProcessError as e:
            occ.writer.write(
                "ERR CODE {}\nON_CMD {}\n".format(str(e.returncode), e.cmd,).encode(
                    "utf-8"
                )
            )
            await occ.writer.drain()
        except FileNotFoundError:
            occ.writer.write(b"ERR FILE_NOT_FOUND\n")
            await occ.writer.drain()

    def is_active(self, appimage_path):
        ops = AppImageOps(appimage_path)
        plm = LifecycleManager.create(ops).privileged
        return plm.is_active()

    def save(self):
        with open(SERVER_CACHE_FILE, "wb") as f:
            pickle.dump(self, f)

    def nginx_template(self):
        return render_nginx_conf(
            tlds=self.domains,
            avahi_hostname="iuvia-io.local",
            services=self.services.values(),
        )

    def reload_nginx(self):
        with open("/etc/nginx/nginx.conf", "wb") as f:
            f.write(self.nginx_template().encode("utf-8"))
        systemctl.reload("nginx.service")


_control_server = None


def get_control_server() -> ControlServer:
    global _control_server
    if _control_server is None:
        if os.path.exists(SERVER_CACHE_FILE):
            with open(SERVER_CACHE_FILE, "rb") as f:
                _control_server = pickle.load(f)  # noqa: S301
        if not _control_server:
            _control_server = ControlServer()
    return _control_server


class OnClientConnected(object):
    def __init__(self, reader: StreamReader, writer: StreamWriter):
        self.reader = reader
        self.writer = writer

    @property
    def server(self):
        return get_control_server()

    @classmethod
    async def create(cls, reader: StreamReader, writer: StreamWriter):
        self = cls(reader=reader, writer=writer)
        await self.begin_session()

    def get_cmd(self, cmd):
        """
        Used internally to signal which commands do exist
        """
        on_cmd = "on_{}".format(cmd)
        if hasattr(self, on_cmd) and callable(getattr(self, on_cmd)):
            return getattr(self, on_cmd)

    async def begin_session(self):
        """
        Begin a session by greeting the client and stating our version.
        Afterwards, await for messages from the client and react to them.
        We read the first line and pass arguments only for the first line.
        Afterwards it's up to each command to implement.

        Only a single command is expected to run here. If there are more commands
        expected to run, those commands may call begin_session to restart again.
        """
        self.writer.write(b"iuvia.control\nversion 1\n")
        await self.writer.drain()
        line = await self.reader.readline()
        cmd = line.decode("utf-8").split()[0]
        try:
            if self.get_cmd(cmd):
                await self.get_cmd(cmd)(line)
            else:
                self.writer.write(b"command-unknown ok-thanks-bye\n")
                await self.writer.drain()
        finally:
            await self.close()

    async def on_version(self, _line):
        self.writer.write(__version__.encode("utf-8"))
        await self.writer.drain()

    async def on_register_service(self, line):
        """
        First line will contain the number of bytes in the payload.
        Afterwards, it will be a JSON payload.
        You need to send the payload in EXACTLY those number of bytes.
        """
        click.echo("Service registration incoming:")
        click.echo(line)
        length = int(line.split()[1])
        msg = await self.reader.readexactly(length)
        msg = json.loads(msg)
        name = msg["name"]
        click.echo("Service to be registered is: {}".format(name))
        self.server.register(**msg)
        self.server.reload_nginx()

    async def on_unregister_service(self, line):
        """
        First line will contain the name of the service (subdomain).
        No more lines should be sent.
        """
        name = line.split()[1].decode('utf-8')
        click.echo("Service to be unregistered is: {}".format(name))
        self.server.unregister(name)
        self.server.reload_nginx()

    async def on_install_appimage(self, line):
        """
        Install an appimage given a local path
        """

        # Remove first element from line
        appimage_path = line.split(b" ")[1:]
        # Then restate all spaces if there were any, and remove trailing newline
        appimage_path = bytes.join(b" ", appimage_path)[:-1].decode("utf-8")
        await self.server.install_appimage(appimage_path, self)

    async def on_uninstall_appimage(self, line):
        """
        Install an appimage given a local path
        """

        # Remove first element from line
        appimage_path = line.split(b" ")[1:]
        # Then restate all spaces if there were any, and remove trailing newline
        appimage_path = bytes.join(b" ", appimage_path)[:-1].decode("utf-8")
        await self.server.uninstall_appimage(appimage_path, self)

    async def on_debug_registered(self, line):
        """
        Get currently registered services
        """
        click.echo("Asking for currently registered services")
        click.echo(str(self.server.services))
        self.writer.write(str(self.server.services).encode("utf-8"))
        await self.close()

    async def on_debug_nginx_conf(self, line):
        self.writer.write(self.server.nginx_template().encode("utf-8"))
        await self.close()

    async def on_bye(self, _line):
        self.writer.write(b"bye")
        await self.writer.drain()

    async def close(self):
        self.writer.write_eof()
        self.writer.close()
        await self.writer.wait_closed()


async def setup_main_loop(loop=None):
    "These are the docs of setup_main_loop"
    from iuvia.control.internal import CONTROL_SOCK_PATH

    sock_dir = os.path.dirname(CONTROL_SOCK_PATH)
    if not os.path.exists(sock_dir):
        os.makedirs(sock_dir)

    systemd.signal_ready()
    fds = systemd.listen_fds(False)
    if fds:  # Activated via systemd.socket
        fds = systemd.SD_LISTEN_FDS_START + fds - 1
        server = await asyncio.streams.start_unix_server(
            client_connected_cb=OnClientConnected.create,
            loop=loop,
            start_serving=True,
            sock=socket.socket(fileno=fds),
        )
    else:
        server = await asyncio.streams.start_unix_server(
            client_connected_cb=OnClientConnected.create,
            path=CONTROL_SOCK_PATH,
            loop=loop,
            start_serving=True,
        )
        os.chmod(CONTROL_SOCK_PATH, 0o666)  # noqa: S103
    await server.serve_forever()


@click.command()
@click.option('--init-nginx/--no-init-nginx', help='Initialize nginx.conf on first run', is_flag=True, default=True)
def cli(init_nginx):
    click.echo("iuvia.control.server")
    check_runtime()
    if init_nginx:
        get_control_server().reload_nginx()
    asyncio.run(setup_main_loop())
