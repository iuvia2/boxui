#!/usr/bin/env python3

import os
from typing import List, Dict, Any

from jinja2 import Environment

JINJA_FILE = os.path.join(os.path.dirname(__file__), "nginx.jinja")


def render_nginx_conf(
    tlds: List[str], avahi_hostname: str, services: List[Dict[str, Any]]
) -> str:
    env = Environment(autoescape=False,)  # noqa: S701
    env.globals["exists"] = os.path.exists
    env.globals["Env"] = os.environ
    with open(JINJA_FILE, "r") as f:
        s = f.read()
        template = env.from_string(s)

    return template.render(tlds=tlds, avahi_hostname=avahi_hostname, services=services,)


def main():
    print(
        render_nginx_conf(
            tlds=["sicily-demo.iuvia.io", "sicily-demo.demo.iuvia.io"],
            avahi_hostname="sicily-demo-iuvia-io.local",
            services=[
                {
                    "name": "boxui",
                    "domain": "sicily-demo.iuvia.io",
                    "path": "/boxui/",
                    "default": 0,
                    "backend": "unix:/run/services/http/boxui.sock",
                }
            ],
        )
    )


if __name__ == "__main__":
    main()
