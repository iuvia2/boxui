# Protocol Buffers

To compile the `.proc` file, run:

```
$ protoc --python_out=. appimage.proto
```
