/**
 * AppImage metadata definition.
 *
 * This protocol describes an AppImage metadata. This is a versioned
 * protocol, and later versions of the protocol may include and remove
 * fields.
 */

syntax = "proto3";

// import "google/protobuf/any.proto";

package io.iuvia;


message AppImage {

  /**
   * Represents requirements for the AppImage.
   *
   * Initially only platform-based requirements will be permitted.
   */
  message Requirement {
    /**
     * Codename of the requirement that needs to exist.
     *
     * E.g., "io.iuvia.platform/chubascos"
     */
    string codename = 1;
    /**
     * Minimum semver-compatible version that can be compatible with
     * the AppImage
     */
    string min_version = 2;

    /**
     *  First semver-compatible version that would be incompatible
     */
    string max_version = 3;
  }

  /**
   * Represents definitions for port exposition and is used by utils
   * to expose those ports to the outside world.
   */
  // TODO: Future implementation
  // message Port {
  // }

  message LocalizedAppStrings {
    string lang = 2;
    string name = 3;
    string short_description = 4;
    string long_description = 5;
  }

  message AppLauncher {

    message LocalizedLauncherStrings {
      string lang = 2;
      string name = 3;
      string description = 4;
    }

    message LauncherCondition {
      string operator = 1;
      // repeated google.protobuf.Any details = 2;
    }

    string platform = 2;
    string on_activate = 3;
    repeated LauncherCondition conditions = 4;
    repeated LocalizedLauncherStrings l10n = 5;
    repeated int32 icon_sizes = 6;
    map<int32, string> icon_locations = 7;
  }

  message PlatformResources {
    string cpu = 1;
    string ram = 2;
  }

  uint32 metadata_version = 1;
  string codename = 2;
  string version = 3;
  bytes developer = 4;
  bytes certificate = 5;
  repeated int32 icon_sizes = 6;
  map<int32, string> icon_locations = 7;
  repeated Requirement requires = 8;
  // repeated Port ports = 9;
  string l10n_default = 10;
  repeated LocalizedAppStrings l10n = 11;
  string license_code = 12;
  string license_text = 13;

  repeated AppLauncher launchers = 14;

  PlatformResources expected_resources = 15;
  PlatformResources max_resources = 16;

}
